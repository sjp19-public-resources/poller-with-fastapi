from db import AutoVOQueries
from fastapi import Depends, FastAPI
from models import AutoVO
from poller import poll
import asyncio

app = FastAPI()

"""
this is just a normal endpoint that lists all of the
AutoVOs that the sales service has
"""
@app.get("/api/autos", response_model=list[AutoVO])
def get_autos(queries: AutoVOQueries=Depends()):
    autos = queries.get_all_autos()
    return autos

"""
The decorator below tells FastAPI to call the following
function once when the server first starts. `schedule_poller()`
creates a new "event loop" which will run independently from
the main FastAPI thread and runs the `poller.poll()` function
inside the loop. Look inside of `poller.poll()` for more details.
"""
@app.on_event("startup")
async def schedule_poller():
    loop = asyncio.get_event_loop()
    loop.create_task(poll())
